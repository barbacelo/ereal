namespace Ereal
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("kartice dobavljaca")]
    public partial class kartice_dobavljaca
    {
        [Required]
        [StringLength(6)]
        public string konto { get; set; }

        [Required]
        [StringLength(6)]
        public string sifra { get; set; }

        [Column(TypeName = "date")]
        public DateTime? valuta { get; set; }

        [Required]
        [StringLength(6)]
        public string redbr { get; set; }

        [Column(TypeName = "date")]
        public DateTime datumpr { get; set; }

        [StringLength(10)]
        public string knjiga { get; set; }

        [Column(TypeName = "date")]
        public DateTime? datumf { get; set; }

        [StringLength(30)]
        public string origin { get; set; }

        [StringLength(25)]
        public string nacinp { get; set; }

        [StringLength(50)]
        public string opis { get; set; }

        public decimal? dug { get; set; }

        public decimal? pot { get; set; }

        [StringLength(20)]
        public string kateg { get; set; }

        [StringLength(1)]
        public string vrsta { get; set; }

        public decimal? saldo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? datump { get; set; }

        [Column(TypeName = "date")]
        public DateTime? datumpl { get; set; }

        [Key]
        public int pk { get; set; }

        public virtual dobavljaci dobavljaci { get; set; }
    }
}
