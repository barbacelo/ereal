namespace Ereal
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ulazni racuni")]
    public partial class ulazni_racuni
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ulazni_racuni()
        {
            realizacijas = new HashSet<realizacija>();
        }

        [Required]
        [StringLength(6)]
        public string konto { get; set; }

        [Required]
        [StringLength(6)]
        public string sifra { get; set; }

        [Column(TypeName = "date")]
        public DateTime? valuta { get; set; }

        [Key]
        public int redbr { get; set; }

        [Column(TypeName = "date")]
        public DateTime datumpr { get; set; }

        [Column(TypeName = "date")]
        public DateTime datumf { get; set; }

        [Required]
        [StringLength(30)]
        public string origin { get; set; }

        [StringLength(25)]
        public string nacinp { get; set; }

        [StringLength(50)]
        public string opis { get; set; }

        public decimal? osnovvs { get; set; }

        public decimal? porezvs { get; set; }

        public decimal? osnovns { get; set; }

        public decimal? porezns { get; set; }

        public decimal? dug { get; set; }

        public decimal? pot { get; set; }

        [StringLength(20)]
        public string kateg { get; set; }

        [Required]
        [StringLength(1)]
        public string vrsta { get; set; }

        public decimal? saldo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? datump { get; set; }

        [Column(TypeName = "date")]
        public DateTime? datumpl { get; set; }

        [Required]
        [StringLength(1)]
        public string pn { get; set; }

        public virtual dobavljaci dobavljaci { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<realizacija> realizacijas { get; set; }
    }
}
