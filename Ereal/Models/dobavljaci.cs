namespace Ereal
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("dobavljaci")]
    public partial class dobavljaci
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public dobavljaci()
        {
            kartice_dobavljaca = new HashSet<kartice_dobavljaca>();
            ulazni_racuni = new HashSet<ulazni_racuni>();
        }

        [Key]
        [Column(Order = 0)]
        [StringLength(6)]
        public string konto { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(6)]
        public string sifra { get; set; }

        [Required]
        [StringLength(50)]
        public string nazf { get; set; }

        [StringLength(30)]
        public string adresa { get; set; }

        [StringLength(25)]
        public string mesto { get; set; }

        [StringLength(5)]
        public string posta { get; set; }

        [StringLength(25)]
        public string zrac { get; set; }

        [StringLength(18)]
        public string telefon { get; set; }

        [StringLength(18)]
        public string mobilni { get; set; }

        [StringLength(30)]
        public string kontakt { get; set; }

        [StringLength(20)]
        public string email { get; set; }

        [Required]
        [StringLength(9)]
        public string pib { get; set; }

        public decimal? saldo { get; set; }

        [StringLength(8)]
        public string mbpov { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<kartice_dobavljaca> kartice_dobavljaca { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ulazni_racuni> ulazni_racuni { get; set; }
    }
}
