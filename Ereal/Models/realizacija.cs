namespace Ereal
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("realizacija")]
    public partial class realizacija
    {
        [Required]
        [StringLength(6)]
        public string konto { get; set; }

        [Required]
        [StringLength(6)]
        public string sifra { get; set; }

        [Required]
        [StringLength(30)]
        public string origin { get; set; }

        [StringLength(6)]
        public string kontor { get; set; }

        public decimal iznos { get; set; }

        [Column(TypeName = "date")]
        public DateTime? datump { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int pk { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int redbr { get; set; }

        public virtual ulazni_racuni ulazni_racuni { get; set; }
    }
}
