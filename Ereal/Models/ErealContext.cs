namespace Ereal
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ErealContext : DbContext
    {
        public ErealContext()
            : base("name=ErealContext")
        {
        }

        public virtual DbSet<dobavljaci> dobavljacis { get; set; }
        public virtual DbSet<kartice_dobavljaca> kartice_dobavljacas { get; set; }
        public virtual DbSet<realizacija> realizacijas { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<ulazni_racuni> ulazni_racunis { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.konto)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.sifra)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.nazf)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.adresa)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.mesto)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.posta)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.zrac)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.telefon)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.mobilni)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.kontakt)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.email)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.pib)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.saldo)
                .HasPrecision(15, 2);

            modelBuilder.Entity<dobavljaci>()
                .Property(e => e.mbpov)
                .IsFixedLength();

            modelBuilder.Entity<dobavljaci>()
                .HasMany(e => e.kartice_dobavljaca)
                .WithRequired(e => e.dobavljaci)
                .HasForeignKey(e => new { e.konto, e.sifra })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<dobavljaci>()
                .HasMany(e => e.ulazni_racuni)
                .WithRequired(e => e.dobavljaci)
                .HasForeignKey(e => new { e.konto, e.sifra })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<kartice_dobavljaca>()
                .Property(e => e.konto)
                .IsFixedLength();

            modelBuilder.Entity<kartice_dobavljaca>()
                .Property(e => e.sifra)
                .IsFixedLength();

            modelBuilder.Entity<kartice_dobavljaca>()
                .Property(e => e.redbr)
                .IsFixedLength();

            modelBuilder.Entity<kartice_dobavljaca>()
                .Property(e => e.knjiga)
                .IsFixedLength();

            modelBuilder.Entity<kartice_dobavljaca>()
                .Property(e => e.origin)
                .IsFixedLength();

            modelBuilder.Entity<kartice_dobavljaca>()
                .Property(e => e.nacinp)
                .IsFixedLength();

            modelBuilder.Entity<kartice_dobavljaca>()
                .Property(e => e.opis)
                .IsFixedLength();

            modelBuilder.Entity<kartice_dobavljaca>()
                .Property(e => e.dug)
                .HasPrecision(15, 2);

            modelBuilder.Entity<kartice_dobavljaca>()
                .Property(e => e.pot)
                .HasPrecision(15, 2);

            modelBuilder.Entity<kartice_dobavljaca>()
                .Property(e => e.kateg)
                .IsFixedLength();

            modelBuilder.Entity<kartice_dobavljaca>()
                .Property(e => e.vrsta)
                .IsFixedLength();

            modelBuilder.Entity<kartice_dobavljaca>()
                .Property(e => e.saldo)
                .HasPrecision(12, 2);

            modelBuilder.Entity<realizacija>()
                .Property(e => e.konto)
                .IsFixedLength();

            modelBuilder.Entity<realizacija>()
                .Property(e => e.sifra)
                .IsFixedLength();

            modelBuilder.Entity<realizacija>()
                .Property(e => e.origin)
                .IsFixedLength();

            modelBuilder.Entity<realizacija>()
                .Property(e => e.kontor)
                .IsFixedLength();

            modelBuilder.Entity<realizacija>()
                .Property(e => e.iznos)
                .HasPrecision(15, 2);

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.konto)
                .IsFixedLength();

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.sifra)
                .IsFixedLength();

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.origin)
                .IsFixedLength();

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.nacinp)
                .IsFixedLength();

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.opis)
                .IsFixedLength();

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.osnovvs)
                .HasPrecision(12, 2);

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.porezvs)
                .HasPrecision(12, 2);

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.osnovns)
                .HasPrecision(12, 2);

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.porezns)
                .HasPrecision(12, 2);

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.dug)
                .HasPrecision(15, 2);

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.pot)
                .HasPrecision(15, 2);

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.kateg)
                .IsFixedLength();

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.vrsta)
                .IsFixedLength();

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.saldo)
                .HasPrecision(12, 2);

            modelBuilder.Entity<ulazni_racuni>()
                .Property(e => e.pn)
                .IsFixedLength();

            modelBuilder.Entity<ulazni_racuni>()
                .HasMany(e => e.realizacijas)
                .WithRequired(e => e.ulazni_racuni)
                .WillCascadeOnDelete(false);
        }
    }
}
