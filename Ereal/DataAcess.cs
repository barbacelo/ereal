﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ereal
{
    public class DAL
    {
        private readonly ErealContext _context = new ErealContext();

        public List<dobavljaci> GetDobavljaci()
        {
            return _context.Set<dobavljaci>().ToList();
        }

        public List<kartice_dobavljaca> GetKartice_dobavljaca()
        {
            return _context.Set<kartice_dobavljaca>().ToList();
        }

        public List<realizacija> GetRealizacija()
        {
            return _context.Set<realizacija>().ToList();
        }

        public List<ulazni_racuni> GetUlazni_racuni()
        {
            return _context.Set<ulazni_racuni>().ToList();
        }
    }
}
