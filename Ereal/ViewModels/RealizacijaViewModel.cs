﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ereal.ViewModels
{
    public class RealizacijaViewModel : BaseViewModel
    {
        private string _konto;
        private string _sifra;
        private string _origin;
        private string _kontor;
        private decimal _iznos;
        private DateTime? _datump;

        private readonly DAL _dal;

        public string Konto
        {
            get => _konto;
            set
            {
                if (value == _konto) return;
                _konto = value;
                OnPropertyChanged();
            }
        }

        public string Sifra
        {
            get => _sifra;
            set
            {
                if (value == _sifra) return;
                _sifra = value;
                OnPropertyChanged();
            }
        }

        public string Origin
        {
            get => _origin;
            set
            {
                if (value == _origin) return;
                _origin = value;
                OnPropertyChanged();
            }
        }

        public string Kontor
        {
            get => _kontor;
            set
            {
                if (value == _kontor) return;
                _kontor = value;
                OnPropertyChanged();
            }
        }

        public decimal Iznos
        {
            get => _iznos;
            set
            {
                if (value == _iznos) return;
                _iznos = value;
                OnPropertyChanged();
            }
        }

        public DateTime? Datump
        {
            get => _datump;
            set
            {
                if (value == _datump) return;
                _datump = value;
                OnPropertyChanged();
            }
        }

        private readonly realizacija _model;

        public RealizacijaViewModel()
        {
            _model = new realizacija();
        }

        public RealizacijaViewModel(realizacija k)
        {
            _model = k;

            Konto = k.konto;
            Sifra = k.sifra;
            Origin = k.origin;
            Kontor = k.kontor;
            Iznos = k.iznos;
            Datump = k.datump;

        }

        public RealizacijaViewModel(DAL dal)
        {
            _dal = dal;
            _model = new realizacija();
        }

        public realizacija GetModel()
        {
            _model.konto = Konto;
            _model.sifra = Sifra;
            _model.origin = Origin;
            _model.kontor = Kontor;
            _model.iznos = Iznos;
            _model.datump = Datump;

            return _model;
        }
    }
}
