﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ereal.ViewModels
{
    public class DobavljaciViewModel : BaseViewModel
    {
        private string _konto;
        private string _sifra;
        private string _nazf;
        private string _adresa;
        private string _mesto;
        private string _posta;
        private string _zrac;
        private string _telefon;
        private string _mobilni;
        private string _kontakt;
        private string _email;
        private string _pib;
        private decimal? _saldo;

        private readonly DAL _dal;

        public string Konto
        {
            get => _konto;
            set
            {
                if (value == _konto) return;
                _konto = value;
                OnPropertyChanged();
            }
        }

        public string Sifra
        {
            get => _sifra;
            set
            {
                if (value == _sifra) return;
                _sifra = value;
                OnPropertyChanged();
            }
        }

        public string Nazf
        {
            get => _nazf;
            set
            {
                if (value == _nazf) return;
                _nazf = value;
                OnPropertyChanged();
            }
        }

        public string Adresa
        {
            get => _adresa;
            set
            {
                if (value == _adresa) return;
                _adresa = value;
                OnPropertyChanged();
            }
        }

        public string Mesto
        {
            get => _mesto;
            set
            {
                if (value == _mesto) return;
                _mesto = value;
                OnPropertyChanged();
            }
        }

        public string Posta
        {
            get => _posta;
            set
            {
                if (value == _posta) return;
                _posta = value;
                OnPropertyChanged();
            }
        }

        public string Zrac
        {
            get => _zrac;
            set
            {
                if (value == _zrac) return;
                _zrac = value;
                OnPropertyChanged();
            }
        }

        public string Telefon
        {
            get => _telefon;
            set
            {
                if (value == _telefon) return;
                _telefon = value;
                OnPropertyChanged();
            }
        }

        public string Mobilni
        {
            get => _mobilni;
            set
            {
                if (value == _mobilni) return;
                _mobilni = value;
                OnPropertyChanged();
            }
        }

        public string Kontakt
        {
            get => _kontakt;
            set
            {
                if (value == _kontakt) return;
                _kontakt = value;
                OnPropertyChanged();
            }
        }

        public string Email
        {
            get => _email;
            set
            {
                if (value == _email) return;
                _email = value;
                OnPropertyChanged();
            }
        }

        public string Pib
        {
            get => _pib;
            set
            {
                if (value == _pib) return;
                _pib = value;
                OnPropertyChanged();
            }
        }

        public decimal? Saldo
        {
            get => _saldo;
            set
            {
                if (value == _saldo) return;
                _saldo = value;
                OnPropertyChanged();
            }
        }

        private readonly dobavljaci _model;

        public DobavljaciViewModel()
        {
            _model = new dobavljaci();
        }

        public DobavljaciViewModel(dobavljaci k)
        {
            _model = k;

            Konto = k.konto;
            Sifra = k.sifra;
            Nazf  = k.nazf;
            Adresa = k.adresa;
            Mesto = k.mesto;
            Posta = k.posta;
            Zrac = k.zrac;
            Telefon = k.telefon;
            Mobilni = k.mobilni;
            Kontakt = k.kontakt;
            Email = k.email;
            Pib = k.pib;
            Saldo = k.saldo;
            
        }

        public DobavljaciViewModel(DAL dal)
        {
            _dal = dal;
            _model = new dobavljaci();
        }

        public dobavljaci GetModel()
        {
            _model.konto = Konto;
            _model.sifra = Sifra;
            _model.nazf = Nazf;
            _model.adresa = Adresa;
            _model.mesto = Mesto;
            _model.posta = Posta;
            _model.zrac = Zrac;
            _model.telefon = Telefon;
            _model.mobilni = Mobilni;
            _model.kontakt = Kontakt;
            _model.email = Email;
            _model.pib = Pib;
            _model.saldo = Saldo;

            return _model;
        }

    }
}
