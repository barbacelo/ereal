﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ereal.ViewModels
{
    public class RealizacijasViewModel : BaseViewModel
    {
        private readonly DAL _dal;

        public ObservableCollection<RealizacijaViewModel> Realizacijas { get; }

        public RealizacijasViewModel(DAL dal)
        {
            _dal = dal;
            Realizacijas = new ObservableCollection<RealizacijaViewModel>(_dal.GetRealizacija().Select(x => new RealizacijaViewModel(x)).ToList());
        }
    }
}
