﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ereal.ViewModels
{
    public class KarticeDobavljacasViewModel : BaseViewModel
    {
        private readonly DAL _dal;

        public ObservableCollection<KarticeDobavljacaViewModel> KarticeDobavljacas { get; }

        public KarticeDobavljacasViewModel(DAL dal)
        {
            _dal = dal;
            KarticeDobavljacas = new ObservableCollection<KarticeDobavljacaViewModel>(_dal.GetKartice_dobavljaca().Select(x => new KarticeDobavljacaViewModel(x)).ToList());
        }
    }
}
