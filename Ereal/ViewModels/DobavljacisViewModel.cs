﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Ereal.ViewModels
{
    public class DobavljacisViewModel : BaseViewModel
    {
        private readonly DAL _dal;
        private DobavljaciViewModel _selectedDobavljaci;
        private DobavljaciViewModel _newDobavljaci;

        public DobavljaciViewModel SelectedDobavljaci
        {
            get => _selectedDobavljaci;
            set
            {
                if (value == _selectedDobavljaci) return;
                _selectedDobavljaci = value;
                OnPropertyChanged();
            }
        }

        public DobavljaciViewModel NewDobavljaci
        {
            get => _newDobavljaci;
            set
            {
                _newDobavljaci = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<DobavljaciViewModel> Dobavljacis { get; }

        public DobavljacisViewModel(DAL dal)
        {
            _dal = dal;
            Dobavljacis = new ObservableCollection<DobavljaciViewModel>(_dal.GetDobavljaci().Select(x => new DobavljaciViewModel(x)).ToList());
        }
    }
}
