﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ereal.ViewModels
{
    public class KarticeDobavljacaViewModel : BaseViewModel
    {
        private string _konto;
        private string _sifra;
        private DateTime? _valuta;
        private string _redbr;
        private DateTime _datumpr;
        private string _knjiga;
        private DateTime? _datumf;
        private string _origin;
        private string _nacinp;
        private string _opis;
        private decimal? _dug;
        private decimal? _pot;
        private string _kateg;
        private string _vrsta;
        private decimal? _saldo;
        private DateTime? _datump;
        private DateTime? _datumpl;

        private readonly DAL _dal;

        public string Konto
        {
            get => _konto;
            set
            {
                if (value == _konto) return;
                _konto = value;
                OnPropertyChanged();
            }
        }

        public string Sifra
        {
            get => _sifra;
            set
            {
                if (value == _sifra) return;
                _sifra = value;
                OnPropertyChanged();
            }
        }

        public DateTime? Valuta
        {
            get => _valuta;
            set
            {
                if (value == _valuta) return;
                _valuta = value;
                OnPropertyChanged();
            }
        }

        public string Redbr
        {
            get => _redbr;
            set
            {
                if (value == _redbr) return;
                _redbr = value;
                OnPropertyChanged();
            }
        }

        public DateTime Datumpr
        {
            get => _datumpr;
            set
            {
                if (value == _datumpr) return;
                _datumpr = value;
                OnPropertyChanged();
            }
        }

        public string Knjiga
        {
            get => _knjiga;
            set
            {
                if (value == _knjiga) return;
                _knjiga = value;
                OnPropertyChanged();
            }
        }

        public DateTime? Datumf
        {
            get => _datumf;
            set
            {
                if (value == _datumf) return;
                _datumf = value;
                OnPropertyChanged();
            }
        }

        public string Origin
        {
            get => _origin;
            set
            {
                if (value == _origin) return;
                _origin = value;
                OnPropertyChanged();
            }
        }

        public string Nacinp
        {
            get => _nacinp;
            set
            {
                if (value == _nacinp) return;
                _nacinp = value;
                OnPropertyChanged();
            }
        }

        public string Opis
        {
            get => _opis;
            set
            {
                if (value == _opis) return;
                _opis = value;
                OnPropertyChanged();
            }
        }

        public decimal? Dug
        {
            get => _dug;
            set
            {
                if (value == _dug) return;
                _dug = value;
                OnPropertyChanged();
            }
        }

        public decimal? Pot
        {
            get => _pot;
            set
            {
                if (value == _pot) return;
                _pot = value;
                OnPropertyChanged();
            }
        }

        public string Kateg
        {
            get => _kateg;
            set
            {
                if (value == _kateg) return;
                _kateg = value;
                OnPropertyChanged();
            }
        }

        public string Vrsta
        {
            get => _vrsta;
            set
            {
                if (value == _vrsta) return;
                _vrsta = value;
                OnPropertyChanged();
            }
        }

        public decimal? Saldo
        {
            get => _saldo;
            set
            {
                if (value == _saldo) return;
                _saldo = value;
                OnPropertyChanged();
            }
        }

        public DateTime? Datump
        {
            get => _datump;
            set
            {
                if (value == _datump) return;
                _datump = value;
                OnPropertyChanged();
            }
        }

        public DateTime? Datumpl
        {
            get => _datumpl;
            set
            {
                if (value == _datumpl) return;
                _datumpl = value;
                OnPropertyChanged();
            }
        }

        private readonly kartice_dobavljaca _model;

        public KarticeDobavljacaViewModel()
        {
            _model = new kartice_dobavljaca();
        }

        public KarticeDobavljacaViewModel(kartice_dobavljaca k)
        {
            _model = k;
            Konto = k.konto;
            Sifra = k.sifra;
            Valuta = k.valuta;
            Redbr = k.redbr;
            Datumpr = k.datumpr;
            Knjiga = k.knjiga;
            Datumf = k.datumf;
            Origin = k.origin;
            Nacinp = k.nacinp;
            Opis = k.opis;
            Dug = k.dug;
            Pot = k.pot;
            Kateg = k.kateg;
            Vrsta = k.vrsta;
            Saldo = k.saldo;
            Datump = k.datump;
            Datumpl = k.datumpl;
        }

        public KarticeDobavljacaViewModel(DAL dal)
        {
            _dal = dal;
            _model = new kartice_dobavljaca();
        }

        public kartice_dobavljaca GetModel()
        {
            _model.konto = Konto;
            _model.sifra = Sifra;
            _model.valuta = Valuta;
            _model.redbr = Redbr;
            _model.datumpr = Datumpr;
            _model.knjiga = Knjiga;
            _model.datumf = Datumf;
            _model.origin = Origin;
            _model.nacinp = Nacinp;
            _model.opis = Opis;
            _model.dug = Dug;
            _model.pot = Pot;
            _model.kateg = Kateg;
            _model.vrsta = Vrsta;
            _model.saldo = Saldo;
            _model.datump = Datump;
            _model.datumpl = Datumpl;

            return _model;
        }

    }
}
