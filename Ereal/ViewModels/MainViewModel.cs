using DevExpress.Mvvm;
using JetBrains.Annotations;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Ereal.ViewModels;

namespace Ereal
{
    public class MainViewModel : INotifyPropertyChanged
    {

        public DobavljacisViewModel Dobavljacis { get; }
        public KarticeDobavljacasViewModel KarticeDobavljacas { get; }
        public RealizacijasViewModel Realizacijas { get; }

        public MainViewModel()
        {
            var dal = new DAL();

            Dobavljacis = new DobavljacisViewModel(dal);
            KarticeDobavljacas = new KarticeDobavljacasViewModel(dal);
            Realizacijas = new RealizacijasViewModel(dal);

        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}