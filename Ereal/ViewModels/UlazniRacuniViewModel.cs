﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ereal.ViewModels
{
    public class UlazniRacuniViewModel : BaseViewModel
    {
        private int _redbr;
        private DateTime _datumpr;
        private string _konto;
        private string _sifra;
        private string _origin;
        private DateTime _datumf;
        private DateTime? _valuta;
        private string _opis;
        private string _nacinp;
        private decimal? _dug;
        private decimal? _pot;
        private decimal? _saldo;
        private string _vrsta;
        private string _kateg;
        private DateTime? _datump;
        private DateTime? _datumpl;
        private decimal? _osnovvs;
        private decimal? _porezvs;
        private decimal? _osnovns;
        private decimal? _porezns;

        private readonly DAL _dal;

        public int Redbr
        {
            get => _redbr;
            set
            {
                if (value == _redbr) return;
                _redbr = value;
                OnPropertyChanged();
            }
        }

        public DateTime Datumpr
        {
            get => _datumpr;
            set
            {
                if (value == _datumpr) return;
                _datumpr = value;
                OnPropertyChanged();
            }
        }

        public string Konto
        {
            get => _konto;
            set
            {
                if (value == _konto) return;
                _konto = value;
                OnPropertyChanged();
            }
        }

        public string Sifra
        {
            get => _sifra;
            set
            {
                if (value == _sifra) return;
                _sifra = value;
                OnPropertyChanged();
            }
        }

        public string Origin
        {
            get => _origin;
            set
            {
                if (value == _origin) return;
                _origin = value;
                OnPropertyChanged();
            }
        }

        public DateTime Datumf
        {
            get => _datumf;
            set
            {
                if (value == _datumf) return;
                _datumf = value;
                OnPropertyChanged();
            }
        }

        public DateTime? Valuta
        {
            get => _valuta;
            set
            {
                if (value == _valuta) return;
                _valuta = value;
                OnPropertyChanged();
            }
        }

        public string Opis
        {
            get => _opis;
            set
            {
                if (value == _opis) return;
                _opis = value;
                OnPropertyChanged();
            }
        }

        public string Nacinp
        {
            get => _nacinp;
            set
            {
                if (value == _nacinp) return;
                _nacinp = value;
                OnPropertyChanged();
            }
        }

        public decimal? Dug
        {
            get => _dug;
            set
            {
                if (value == _dug) return;
                _dug = value;
                OnPropertyChanged();
            }
        }

        public decimal? Pot
        {
            get => _pot;
            set
            {
                if (value == _pot) return;
                _pot = value;
                OnPropertyChanged();
            }
        }

        public decimal? Saldo
        {
            get => _saldo;
            set
            {
                if (value == _saldo) return;
                _saldo = value;
                OnPropertyChanged();
            }
        }

        public string Vrsta
        {
            get => _vrsta;
            set
            {
                if (value == _vrsta) return;
                _vrsta = value;
                OnPropertyChanged();
            }
        }

        public string Kateg
        {
            get => _kateg;
            set
            {
                if (value == _kateg) return;
                _kateg = value;
                OnPropertyChanged();
            }
        }

        public DateTime? Datump
        {
            get => _datump;
            set
            {
                if (value == _datump) return;
                _datump = value;
                OnPropertyChanged();
            }
        }

        public DateTime? Datumpl
        {
            get => _datumpl;
            set
            {
                if (value == _datumpl) return;
                _datumpl = value;
                OnPropertyChanged();
            }
        }

        public decimal? Osnovvs
        {
            get => _osnovvs;
            set
            {
                if (value == _osnovvs) return;
                _osnovvs = value;
                OnPropertyChanged();
            }
        }

        public decimal? Porezvs
        {
            get => _porezvs;
            set
            {
                if (value == _porezvs) return;
                _porezvs = value;
                OnPropertyChanged();
            }
        }

        public decimal? Osnovns
        {
            get => _osnovns;
            set
            {
                if (value == _osnovns) return;
                _osnovns = value;
                OnPropertyChanged();
            }
        }

        public decimal? Porezns
        {
            get => _porezns;
            set
            {
                if (value == _porezns) return;
                _porezns = value;
                OnPropertyChanged();
            }
        }

        private readonly ulazni_racuni _model;

        public UlazniRacuniViewModel()
        {
            _model = new ulazni_racuni();
        }

        public UlazniRacuniViewModel(ulazni_racuni k)
        {
            _model = k;

            Redbr = k.redbr;
            Datumpr = k.datumpr;
            Konto = k.konto;
            Sifra = k.sifra;
            Origin = k.origin;
            Datumf = k.datumf;
            Valuta = k.valuta;
            Opis = k.opis;
            Nacinp = k.nacinp;
            Dug = k.dug;
            Pot = k.pot;
            Saldo = k.saldo;
            Vrsta = k.vrsta;
            Kateg = k.kateg;
            Datump = k.datump;
            Datumpl = k.datumpl;
            Osnovvs = k.osnovvs;
            Porezvs = k.porezvs;
            Osnovns = k.osnovns;
            Porezns = k.porezns;

        }

        public UlazniRacuniViewModel(DAL dal)
        {
            _dal = dal;
            _model = new ulazni_racuni();
        }

        public ulazni_racuni GetModel()
        {
            _model.redbr = Redbr;
            _model.datumpr = Datumpr;
            _model.konto = Konto;
            _model.sifra = Sifra;
            _model.origin = Origin;
            _model.datumf = Datumf;
            _model.valuta = Valuta;
            _model.opis = Opis;
            _model.nacinp = Nacinp;
            _model.dug = Dug;
            _model.pot = Pot;
            _model.saldo = Saldo;
            _model.vrsta = Vrsta;
            _model.kateg = Kateg;
            _model.datump = Datump;
            _model.datumpl = Datumpl;
            _model.osnovvs = Osnovvs;
            _model.porezvs = Porezvs;
            _model.osnovns = Osnovns;
            _model.porezns = Porezns;

            return _model;
        }

    }
}
